
import React, { Component } from 'react';
import './SMSForm.css';
import pic from './pink.jpeg';
import pic1 from './blue.jpeg';
import pic2 from './green.jpeg';

class SMSForm extends Component {
  constructor(props) {
    super(props);
    this.state = {
      message: {
        to: '',
        body: ''
      },
      submitting: false,
      error: false
    };
    this.onSubmit = this.onSubmit.bind(this);
    this.onHandleChange = this.onHandleChange.bind(this);
  }

  onSubmit(event) {
    event.preventDefault();
    this.setState({ submitting: true });
    fetch('/api/messages', {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json'
      },
      body: JSON.stringify(this.state.message)
    })
      .then(res => res.json())
      .then(data => {
        if (data.success) {
          this.setState({
            error: false,
            submitting: false,
            message: {
              to: '',
              body: ''
            }
          });
        } else {
          this.setState({
            error: true,
            submitting: false
          });
        }
      });
  }

  onHandleChange(event) {
    const name = event.target.getAttribute('name');
    this.setState({
      message: { ...this.state.message, [name]: event.target.value }
    });
  }

  render() {
    return (
      <body>
      	<h1>Grocery Pal (UCSC)</h1>

      <p><br/><img src={pic} className= "default-pic" alt = "pink pic"
      width = "100"
      height = "100"/>  WINDOW: 15 mins | ITEM LIMIT: 5 | COST: $4 </p><p> <a href = "https://www.google.com/maps/dir/36.9923139,-122.0581762/trader+joe.s/@36.9772965,-122.0812656,13z/data=!3m1!4b1!4m9!4m8!1m1!4e1!1m5!1m1!1s0x808e402673514d0d:0x894134895d42aa7e!2m2!1d-122.0246976!2d36.974544" > STORE: Trader Joe's (Front St.)</a></p><p>CONTACT INFO: 925-123-3456</p>

      <p><br/><img src={pic1} className= "default1-pic" alt = "blue pic"
      width = "100"
      height = "100"/>  WINDOW: 30 mins | ITEM LIMIT: 7 | COST: $5 </p><p>  <a href = "https://www.google.com/maps/dir//safeway/data=!4m6!4m5!1m1!4e2!1m2!1m1!1s0x808e6a792c351007:0x95a051c16055e645?sa=X&ved=2ahUKEwjZ5eXBt7X1AhX1IkQIHcyJDnkQ9Rd6BAgDEAQ" > STORE: Safeway (Soquel Ave.) </a> </p><p>CONTACT INFO: 925-878-0694</p>

      <p><br/><img src={pic2} className= "default2-pic" alt = "green pic"
      width = "100"
      height = "100"/>  WINDOW: 35 mins | ITEM LIMIT: 5 | COST: $7 </p><p> <a href = "https://www.google.com/maps/dir//whole+foods/data=!4m6!4m5!1m1!4e2!1m2!1m1!1s0x808e401f4a86e31f:0x253cf657929de37?sa=X&ved=2ahUKEwjym4mWuLX1AhV-H0QIHWeiD9AQ9Rd6BAgFEAQ"> STORE: Whole Foods (Soquel St.) </a> </p><p>CONTACT INFO: 925-888-3947</p>

      

      <form
        onSubmit={this.onSubmit}
        className={this.state.error ? 'error sms-form' : 'sms-form'}
      >
        <div>
          <label htmlFor="to">To:</label>
          <input
            type="tel"
            name="to"
            id="to"
            value={this.state.message.to}
            onChange={this.onHandleChange}
          />
        </div>
        <div>
          <label htmlFor="body">Grocery Items:</label>
          <textarea
            name="body"
            id="body"
            value={this.state.message.body}
            onChange={this.onHandleChange}
          />
        </div>
        <button type="submit" disabled={this.state.submitting}>
          Send message
        </button>
      </form>
      </body>
    );
  }
}

export default SMSForm;